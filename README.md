# docker-sruv-postgres

This is minimal docker image based on Alpine Linux and PostgreSQL.

### Initial run
```console
docker run -td -v /var/lib/postgresql/data:/var/lib/postgresql/data:Z -u postgres docker-sruv-postgres initdb
```

### Regular run
```console
docker run -td -v /var/lib/postgresql/data:/var/lib/postgresql/data:Z -u postgres docker-sruv-postgres postgres
```
